import java.sql.*;

public class main {

	 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	 
	 static final String DB_PASS = "jdbc:mysql://localhost/MAMA";
	 
	 static final String USER = "root";
	 static final String PASSWORD = "";
	 
	 public static void main(String [] args) throws SQLException {
		 
		 
		 Connection connection = null;
		 Statement statement = null;
		 
		 try {
			 Class.forName("com.mysql.jdbc.Driver");
			 System.out.println("connecting to the selected database");
			 
			 connection = DriverManager.getConnection(DB_PASS, USER, PASSWORD);
			 
			 System.out.println("connected to database");
			 
			 
			 System.out.println("Inserting records to database");
			 statement = connection.createStatement();
			 
//			 String sql = "INSERT INTO Registration " +
//					 		"VALUES(100, 'Zara', 'abi', 18)";
//			 sql = "INSERT INTO Registration " + " (120, 'ana', 'tiku', 20)";
//			 statement.executeUpdate(sql);
//			 System.out.println("Inserted Successs into table");
//			 
			 String sql2 = "Update Registration " +
					 "Set age = 30 where id = 100";
			 
			 statement.executeUpdate(sql2);
			 
			 String sql23 = "Delete from Registration where id = 101";
			 statement.executeUpdate(sql23);
			 
			 String selectSQL = "Select * from Registration";
			 ResultSet rs = statement.executeQuery(selectSQL);
			 
			 while(rs.next()) {
				 //retrive columns
				 int id = rs.getInt("id");
				 int age = rs.getInt("age");
				 String name = rs.getString("first");
				 String last = rs.getString("last");
				 
				 
				 System.out.println( " ID " + id);
				 System.out.println(", Age " +age);
				 System.out.println(", first " + name);
				 System.out.println(", last " + last);
			 }
			 rs.close();
			 
		}catch (SQLException  se) {
			se.printStackTrace();
			
		}
		 catch (Exception e) {
			e.printStackTrace();
		 }
		 finally {
			 if(connection != null) {
				 connection.close();
			 }
		}
		 System.out.println("GoodBye");
	 }
}
